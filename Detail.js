import React,{Component} from "react";

class Detail extends Component{
    render(){
        const item = this.props.item;
        return(
       <React.Fragment>
           {item ? item : "Khong co item show"}
       </React.Fragment>
        );
    }
}
export default Detail;

/*
 <div className="col-lg-7 col-xl-9 bg-body-dark">
            <div className="content">
                <div className="block block-fx-pop">
                    <div className="block-content block-content-sm block-content-full bg-body-light">
                        <div className="media py-3">
                            <div className="mr-3 ml-2 overlay-container overlay-right">
                                <img className="img-avatar img-avatar48" src="assets/media/avatars/avatar7.jpg" alt=""/>
                                <i className="far fa-circle text-white overlay-item font-size-sm bg-success rounded-circle"></i>
                            </div>
                            <div className="media-body">
                                <div className="row">
                                    <div className="col-sm-7">
                                        <a className="font-w600 link-fx" href="javascript:void(0)">Mary Taylor</a>
                                        <div className="font-size-sm text-muted">m.taylor@example.com</div>
                                    </div>
                                    <div className="col-sm-5 d-sm-flex align-items-sm-center">
                                        <div className="font-size-sm font-italic text-muted text-sm-right w-100 mt-2 mt-sm-0">
                                            <p className="mb-0">April 2, 2018</p>
                                            <p className="mb-0">12:30</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="block-content">
                        <p>Dear Admin,</p>
                        <p>Potenti elit lectus augue eget iaculis vitae etiam, ullamcorper etiam bibendum ad feugiat magna accumsan dolor, nibh molestie cras hac ac ad massa, fusce ante convallis ante urna molestie vulputate bibendum tempus ante justo arcu erat accumsan adipiscing risus, libero condimentum venenatis sit nisl nisi ultricies sed, fames aliquet consectetur consequat nostra molestie neque nullam scelerisque neque commodo turpis quisque etiam egestas vulputate massa, curabitur tellus massa venenatis congue dolor enim integer luctus, nisi suscipit gravida fames quis vulputate nisi viverra luctus id leo dictum lorem, inceptos nibh orci.</p>
                        <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                        <p>All the best,</p>
                        <p>Mary</p>
                        </div>
                        <div className="block-content bg-body-light">
                        <div className="row gutters-tiny">
                            <div className="col-4 col-xl-2">
                                <div className="options-container fx-item-rotate-r">
                                    <img className="img-fluid options-item" src="assets/media/photos/photo16.jpg" alt=""/>
                                    <div className="options-overlay bg-black-75">
                                        <div className="options-overlay-content">
                                            <a className="btn btn-sm btn-primary" href="javascript:void(0)">
                                                <i className="fa fa-download"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <p className="font-size-sm text-muted pt-2">
                                    <i className="fa fa-paperclip"></i> 1a.jpg (785Kb)
                                </p>
                            </div>
                        <div className="col-4 col-xl-2">
                            <div className="options-container fx-item-rotate-r">
                                <img className="img-fluid options-item" src="assets/media/photos/photo4.jpg" alt=""/>
                                <div className="options-overlay bg-black-75">
                                    <div className="options-overlay-content">
                                        <a className="btn btn-sm btn-primary" href="javascript:void(0)">
                                            <i className="fa fa-download"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <p className="font-size-sm text-muted pt-2">
                                <i className="fa fa-paperclip"></i> 1b.jpg (685kb)
                            </p>
                            </div>
                            <div className="col-4 col-xl-2">
                                <div className="options-container fx-item-rotate-r">
                                    <img className="img-fluid options-item" src="assets/media/photos/photo9.jpg" alt=""/>
                                    <div className="options-overlay bg-black-75">
                                        <div className="options-overlay-content">
                                            <a className="btn btn-sm btn-primary" href="javascript:void(0)">
                                                <i className="fa fa-download"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <p className="font-size-sm text-muted pt-2">
                                    <i className="fa fa-paperclip"></i> 1c.jpg (698kb)
                                </p>
                            </div>
                        </div>
                    </div>
                    </div>
                        <div className="block block-fx-pop">
                            <div className="block-content block-content-full">
                                <div className="js-summernote" data-height="100"></div>
                                <button type="button" className="btn btn-hero-sm btn-hero-primary mt-2">Send</button>
                            </div>
                        </div>
                    </div>
            </div>
        </div>    
*/